import React from 'react';
import styled from "styled-components";
import {Flex, Box} from "reflexbox/styled-components";
import {NavLink} from "react-router-dom";

import {withLocalizeStrings} from "../languages/Localize";
import routes from "../config/routes";
import fallbackImage from "../assets/fallbackImage";

const StyledTitle = styled(Box)`
    width: 100%;
    font-size: 16px;
    font-weight: 700;
`;

const StyledDescription = styled(Box)`
    width: 100%;
    font-size: 12px;
`;


const StyledTitleLink = styled(NavLink)`
    width: 100%;
    font-size: 16px;
    display: flex;
    justify-content: flex-end;
`;

const StyledImage = styled.img`
    width: auto;
    height: 120px;
`;

const StyledCardGrid = styled(Flex)`
    border: 1px solid black;
    border-radius: 16px;
    flex-direction: column;
    padding: 8px 42px 8px 8px;
    height: fit-content;
    width: 270px;
`;

const NewsCard = ({strings, ...props}) => {
    const image = props.article.urlToImage ? props.article.urlToImage : fallbackImage.image;
    return (
        <StyledCardGrid>
            <StyledTitle>{props.article.title}</StyledTitle>
            <StyledImage src={image}></StyledImage>
            <StyledDescription>{props.article.description}</StyledDescription>
            <StyledTitleLink to={{pathname: routes.TOPNEWS_DETAILS, state: props.article}}>{strings.card.MORE}</StyledTitleLink>
        </StyledCardGrid>
    );
}

export default withLocalizeStrings(NewsCard);