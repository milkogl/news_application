import React from "react";

import styled from "styled-components";
import {Flex, Box} from "reflexbox/styled-components";

import {withLocalizeStrings} from "../languages/Localize";
import fallbackImage from "../assets/fallbackImage";
import {useApplicationStateValue} from "../application/Application";

const StyledArticleContainer = styled(Flex)`
    height: 100%;
    flex-direction: column;
    padding: 16px;
    overflow-y: auto;
    ${({theme}) => theme.mediaQueries.medium} {
        padding: 16px 70px 16px 70px;
    }
`;

const StyledTitle = styled(Box)`
    width: 100%;
    font-weight: 700;
    font-size: 16px;
    ${({theme}) => theme.mediaQueries.medium} {
        font-size: 30px;
    }
`;

const StyledImage = styled.img`
    width: auto;
    height: 30vw;
    align-self: center;
    margin-top: 16px;
    ${({theme}) => theme.mediaQueries.medium} {
        height: 15vw;
    }
`;

const StyledContent = styled(Box)`
    width: 100%;
    font-size: 16px;
    margin-top: 16px;
`;

const StyledBack = styled(Box)`
    width: 100%;
    font-size: 16px;
    margin-top: 16px;
    cursor: pointer;
    &:active, &:hover {
        text-decoration: none;
        outline: 0;
    }
    
    &:active {
        color: #096dd9;
    }
    &:hover {
        color: #40a9ff;
    }
`;

const Article = ({strings, ...props}) => {
    const {setLanguageSelectorDisabled} = useApplicationStateValue();
    setLanguageSelectorDisabled(true);
    const article = props.location.state;
    const image = article.urlToImage ? article.urlToImage : fallbackImage.image;

    React.useEffect(() => {
        return () => {
            setLanguageSelectorDisabled(false);
        }
    }, []);

    return (
        <StyledArticleContainer>
            <StyledTitle>{article.title}</StyledTitle>
            <StyledImage src={image}></StyledImage>
            <StyledContent>{article.content}</StyledContent>
            <StyledBack onClick={() => {props.history.goBack()}}>{strings.article.BACK }</StyledBack>
        </StyledArticleContainer>
    )
}

export default withLocalizeStrings(Article);