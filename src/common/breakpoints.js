export default {
    SMALL_PHONES: "567px",
    PHONES: "768px",
    TABLETS: "992px",
    SMALL_LAPTOPS: "1200px"
};