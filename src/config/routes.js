export default {
    HOME: "/",
    TOPNEWS: "/topnews",
    CATEGORIES: "/categories",
    CATEGORY: "/category/",
    SEARCH: "/search",
    TOPNEWS_DETAILS:  "/topnews/details"
};
