import { presetPalettes } from "@ant-design/colors";
import breakpoints from "../common/breakpoints";

const bpsArray = Object.entries(breakpoints).reduce((arr, bp) => {
    arr.push(bp[1]);
    // eslint-disable-next-line no-param-reassign
    arr[bp[0]] = bp[1];
    return arr;
}, []);

const theme = {
    breakpoints: bpsArray,
    mediaQueries: {
        small: `@media screen and (min-width: ${breakpoints['SMALL_PHONES']})`,
        medium: `@media screen and (min-width: ${breakpoints['PHONES']})`,
        mediumMax: `@media screen and (max-width: ${breakpoints['PHONES']})`
    },
    colors: {
        primary: "#025778",
        white: "#fff",
        gray:"#9e9e9e",
        borders: "#e8e8e8",
        orange: "#e66723",
        warning: "#f5a623",
        error: "#E31C12",
        iceBlue: "#eceded",
        green: "#569B07",
        dark: "#000",
        palette: presetPalettes,
    },
    loading: {
        minCardHeight: 100,
    },
    space: [0, 4, 8, 16, 24, 32, 40, 48, 56, 64, 128, 256],
    sizes: [16,32,40,48,56,64],
    fonts: {
        sans: '"myriad-pro", "Helvetica Neue", Helvetica, Arial, sans-serif',
        body: '"myriad-pro", "Helvetica Neue", Helvetica, Arial, sans-serif',
        heading: '"myriad-pro", "Helvetica Neue", Helvetica, Arial, sans-serif',
        mono: "Menlo, monospace",
    },
    fontSizes: [10,12, 13, 16,18, 21, 24, 32, 40, 44, 48],
    fontWeights: {
        body: 400,
        heading: 700,
        normal: 400,
        semiBold: 600,
        bold: 700,
        semiNormal:200,
        actualPanelHeading:450,
        pageHeader:350
    }
};

export default theme;
