import fetch from 'axios';
import {config as getConfig} from '../../environments/environments';

const baseUrl = getConfig().api.endpoint;
const baseConfig = {
    baseURL: baseUrl
};

const createInstance = (config) => {

    return fetch.create(config);
};


const instance = createInstance(baseConfig);

export default {
    baseUrl,
    service() {
        return instance;
    }
}
