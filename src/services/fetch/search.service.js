import fetchAPI from "./fetch.service";

export const getSearchData = (region, searchText) => {
    return fetchAPI.service().get(`/top-headlines?country=${region}&q=${searchText}&apiKey=8a36628a4ca24b86a483b521d32b530a`).then( data => data);
};
