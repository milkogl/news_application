export const appDataMockUp = {
    status: null,
    totalResults: 38,
    articles: [
        {
            source: {
                id: null,
                name: null
            },
            author: null,
            title: null,
            description: null,
            url: null,
            urlToImage: null,
            publishedAt: null,
            content: null
        }
    ]
}

