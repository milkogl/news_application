import fetchAPI from "../services/fetch/fetch.service";

export const getApplicationData = (region) => {
    return fetchAPI.service().get(`/top-headlines?country=${region}&pageSize=100&apiKey=8a36628a4ca24b86a483b521d32b530a`).then( data => data);
};

export const getCategoryData = (region, category) => {
    return fetchAPI.service().get(`/top-headlines?country=${region}&pageSize=100&category=${category}&apiKey=8a36628a4ca24b86a483b521d32b530a`).then( data => data);
};