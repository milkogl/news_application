import React, {useState} from "react";
import PropTypes from "prop-types";
import {withLocalizeStrings} from "../languages/Localize";
import {
    getApplicationData,
    getCategoryData
} from "./application.service";
import { withRouter} from 'react-router';
import {appDataMockUp} from "./appData.mockup";


const defaultState = {
    appData: {
        ...appDataMockUp
    },
    loading: null,
    region: 'gb'
}

const defaultActions = {
    setLoading: _payload => {}
}

export const ApplicationContext = React.createContext({
    ...defaultState,
    ...defaultActions
});
export const ApplicationConsumer = ApplicationContext.Consumer;


const ApplicationProvider = props => {

    const categoryNames = ["business", "entertainment", "general", "health", "science", "sports", "technology"];
    const defaultCollapsedCategories = {
        "business": false, "entertainment": false, "general": false, "health": false, "science": false, "sports": false, "technology": false
    };

    const [appData,setAppData] = useState(null);
    const [loading,setLoading] = useState(false);
    const [region, setRegion] = useState('gb');
    const [categories, setCategories] = useState(null);
    const [collapsedCategories, setCollapsedCategories] = useState(defaultCollapsedCategories);
    const [languageSelectorDisabled, setLanguageSelectorDisabled] = useState(false);

    const fetchAppData = region => {
        setLoading(true);
        getApplicationData(region).then(({data}) => {
            setAppData({...data});
        });

        const categories = {};
        const forLoop = async _ => {
            for (let index = 0; index < categoryNames.length; index++) {
                const category = categoryNames[index];
                const response = await getCategoryData(region, category);
                categories[category] = response.data.articles;
            }
            setLoading(false);
            setCategories(categories);
        }
        forLoop();
    }


    React.useEffect(() => {
        fetchAppData(region);
    }, [region]);

    const state = {
        appData,
        loading,
        region,
        categories,
        collapsedCategories,
        languageSelectorDisabled,
        setLoading,
        setRegion,
        setCategories,
        setCollapsedCategories,
        setLanguageSelectorDisabled
    };

    return (
        <ApplicationContext.Provider value={state}>
            {appData === null ? null : props.children}
        </ApplicationContext.Provider>
    );
};

ApplicationProvider.propTypes = {
    children: PropTypes.node.isRequired,
};
export const useApplicationStateValue = () => React.useContext(ApplicationContext);

export default withRouter(withLocalizeStrings(ApplicationProvider));
