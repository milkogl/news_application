import React, { useState } from "react";
import {Route, Switch, withRouter} from "react-router-dom";
import 'antd/dist/antd.css';
import {ThemeProvider} from "styled-components";
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";

import theme from '../theme/theme';
import routes from "../config/routes.js";
import TopNews from "../top-news/TopNews";
import Article from "../article/Article";
import { LocalizeProvider } from "../languages/Localize";
import Toolbar from "../toolbar/Toolbar";
import SideDrawer from "../toolbar/side-drawer/SideDrawer";
import ApplicationProvider from "../application/Application";
import Categories from "../categories/Categories";
import Search from "../search/Search";
import Category from "../categories/category/Category";

const StyledContentBox = styled(Flex)`
    position: fixed;
    top: 56px;
    z-index: 5;
    height: calc(100% - 56px);
    width: 100%;
    overflow-y: auto;
    padding: 16px;
`;

const Main = () => {

    const [showSideDrawer, setShowSideDrawer] = useState(false);

    const sideDrawerToggleHandler = () => {
        setShowSideDrawer(!showSideDrawer);
    }

    const sideDrawerClosedHandler = () => {
        setShowSideDrawer(false);
    }

    return (
        <>
            <ThemeProvider theme={theme}>
                <LocalizeProvider>
                    <ApplicationProvider>
                        <Toolbar drawerToggleClicked={sideDrawerToggleHandler}></Toolbar>
                        <SideDrawer show={showSideDrawer} closed={sideDrawerClosedHandler}/>
                        <StyledContentBox>
                            <Switch>
                                <Route exact path={routes.HOME} component={TopNews}/>
                                <Route exact path={routes.TOPNEWS} component={TopNews}/>
                                <Route exact path={routes.CATEGORIES} component={Categories}/>
                                <Route exact path={routes.SEARCH} component={Search}/>
                                <Route exact path={routes.TOPNEWS_DETAILS} component={Article}/>
                                <Route exact path={`${routes.CATEGORY}:category`} component={Category}/>
                            </Switch>
                        </StyledContentBox>
                    </ApplicationProvider>
                </LocalizeProvider>
            </ThemeProvider>
        </>
);
};

export default withRouter(Main);
