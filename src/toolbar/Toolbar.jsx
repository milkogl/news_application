import React from 'react';
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";

import Navigation from './navigation/Navigation';
import LanguageSelector from './language-selector/LanguageSelector';
import DrawerToggle from "./drawer-toggle/DrawerToggle";

const StyledToolbar = styled(Flex)`
    height: 56px;
    width: 100%;
    position: fixed;
    top: 0;
    left: 0;
    justify-content: space-between;
    align-items: center;
    box-sizing: border-box;
    z-index: 90;
    border-bottom: 1px solid #00000014;
`;

const StyledContainer = styled(Flex)`
    height: 100%;
    width: 100%;
    justify-content: space-between;
    align-items: center;
    box-sizing: border-box;
    flex-flow: column;
    ${({theme}) => theme.mediaQueries.medium} {
        flex-flow: row;
    }
    ${({theme}) => theme.mediaQueries.mediumMax} {
        display: none;
    }
`;

const Toolbar = (props) => (
    <StyledToolbar>
        <DrawerToggle clicked={props.drawerToggleClicked}/>
        <StyledContainer>
            <Navigation></Navigation>
            <LanguageSelector></LanguageSelector>
        </StyledContainer>
    </StyledToolbar>
)

export default Toolbar;