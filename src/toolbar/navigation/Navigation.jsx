import React from 'react';
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";
import {NavLink, withRouter} from "react-router-dom";

import routes from "../../config/routes";
import {withLocalizeStrings} from "../../languages/Localize";

const StyledNavigation = styled(Flex)`
    height: 100%;
    width: 100%;
    flex-flow: column;
    ${({theme}) => theme.mediaQueries.medium} {
        flex-flow: row;
        width: 30vw;
    }
`;
const StyledNavigationItem = styled(NavLink)`
 width: 100%;
 box-sizing: border-box;
 display: flex;
 height: 100%;
 border: 1px solid ${({theme}) => theme.colors.borders};
 border-bottom: 4px solid transparent;
 border-top:7px solid transparent;
 flex-direction: column;
 text-decoration: none;
 vertical-align: top;
 text-align: center;
 color:${({theme}) => theme.colors.primary};
 justify-content: center;
 align-items: center;
 font-size: ${({theme}) => theme.fontSizes[3]}px;
 user-select: none;
 
 &:hover,&:active,&.active {
    background-color: #b4bcbc;

 margin-bottom: 0;
 color:${({theme}) => theme.colors.primary};
 }`;



const Navigation = ({strings}) => {
    return (<StyledNavigation bg='white'>
        <StyledNavigationItem to={routes.TOPNEWS} exact>{strings.toolbar.TOP_NEWS}</StyledNavigationItem>
        <StyledNavigationItem to={routes.CATEGORIES} exact>{strings.toolbar.CATEGORIES}</StyledNavigationItem>
        <StyledNavigationItem to={routes.SEARCH} exact>{strings.toolbar.SEARCH}</StyledNavigationItem>
    </StyledNavigation>);
}



export default withRouter(withLocalizeStrings(Navigation));
