import React from 'react';
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";

const StyledDrawerToggle = styled(Flex)`
    width: 40px;
    height: 100%;
    flex-flow: column;
    justify-content: space-around;
    align-items: center;
    padding: 10px 0;
    box-sizing: border-box;
    cursor: pointer;
    ${({theme}) => theme.mediaQueries.medium} {
        display: none;
    }
`;

const StyledDiv = styled.div`
    width: 90%;
    height: 3px;
    background-color: black;
`;

const drawerToggle = (props) => (
    <StyledDrawerToggle onClick={props.clicked}>
        <StyledDiv/>
        <StyledDiv/>
        <StyledDiv/>
    </StyledDrawerToggle>
)

export default drawerToggle;