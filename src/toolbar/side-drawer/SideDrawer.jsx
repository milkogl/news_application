import React from 'react';
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";

import Navigation from "../navigation/Navigation";
import LanguageSelector from "../language-selector/LanguageSelector";
import Backdrop from "../../UI/backdrop/Backdrop";

const StyledContainer = styled(Flex)`
    position: fixed;
    width: 280px;
    max-width: 70%;
    height: 100%;
    left: 0;
    top: 0;
    z-index: 200;
    background-color: white;
    box-sizing: border-box;
    transition: transform 0.3s ease-out;
    flex-flow: column;
    ${({theme}) => theme.mediaQueries.medium} {
        display: none;
    }
    &.open {
        transform: translateX(0);
    }
    
    &.close {
        transform: translateX(-100%);
    }
`;

const SideDrawer = (props) => {

    let attachedClass = "close";
    if (props.show) {
        attachedClass = "open";
    }

    return (
    <> 
        <Backdrop show={props.show} clicked={props.closed}/>
        <StyledContainer className={attachedClass} onClick={props.closed}>
                <Navigation></Navigation>
                <LanguageSelector></LanguageSelector>
        </StyledContainer>
    </>
    )
}

export default SideDrawer;