import React from 'react';
import styled from "styled-components";
import {Flex, Box} from "reflexbox/styled-components";
import {withLocalizeStrings} from "../../languages/Localize";
import {useApplicationStateValue} from "../../application/Application";

const StyledLanguageSelector = styled(Flex)`
    cursor: ${({disabled}) => disabled ? "not-allowed" : "pointer"};
    height: 100%;
    width: 100%;
    flex-flow: column;

    ${({theme}) => theme.mediaQueries.medium} {
        flex-flow: row;
        width: 20vw;
    }
`;
const StyledLanguageSelectorItem = styled(Box)`
 width: 100%;
 box-sizing: border-box;
 display: flex;
 height: 100%;
 border: 1px solid ${({theme}) => theme.colors.borders};
 border-bottom: 4px solid transparent;
 border-top:7px solid transparent;
 flex-direction: column;
 text-decoration: none;
 vertical-align: top;
 text-align: center;
 color:${({theme}) => theme.colors.primary};
 justify-content: center;
 align-items: center;
 font-size: ${({theme}) => theme.fontSizes[3]}px;
 user-select: none;
 background: ${({active}) => active ? "#b4bcbc" : "white"};
 pointer-events: ${({disabled}) => disabled ? "none" : "auto"};
 
 &:hover,&:active,&.active {
    background-color: #b4bcbc;
    margin-bottom: 0;
    color:${({theme}) => theme.colors.primary};
 }`;

const LanguageSelector = ({strings}) => {

    const {region, setRegion, languageSelectorDisabled} = useApplicationStateValue();

    return (<StyledLanguageSelector disabled={languageSelectorDisabled} bg='white'>
        <StyledLanguageSelectorItem disabled={languageSelectorDisabled} active={region === 'gb'} onClick={() => {setRegion('gb')}}>{strings.toolbar.GB}</StyledLanguageSelectorItem>
        <StyledLanguageSelectorItem disabled={languageSelectorDisabled} active={region === 'us'} onClick={() => {setRegion('us')}}>{strings.toolbar.US}</StyledLanguageSelectorItem>
    </StyledLanguageSelector>);
}

export default withLocalizeStrings(LanguageSelector);