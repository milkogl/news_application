import React from "react";

import styled from "styled-components";
import {Flex, Box} from "reflexbox/styled-components";
import NewsCard from "../news-card/NewsCard";
import {useApplicationStateValue} from "../application/Application";
import {withLocalizeStrings} from "../languages/Localize";
import Heading from "../components/Heading";

const StyledContainer = styled(Flex)`
    flex-flow: column;
    width: 100%;
`;

const StyledTopNewsContainer = styled(Box)`
    height: 100%;
    width: 100%;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(270px, 0fr));
    grid-gap: 1rem;
    justify-content: center;
`;

const TopNews = ({strings}) => {
    const {appData, region} = useApplicationStateValue();

    const news = appData.articles.map((article, idx)=> {
        return <NewsCard key={idx} article={article}/>;
    })
    const country = region === 'gb' ? strings.GB : strings.US; 
    return (
        <StyledContainer>
            <Heading>{`${strings.topnews.TITLE} ${country}`}</Heading>
            <StyledTopNewsContainer>
                {news}
            </StyledTopNewsContainer>
        </StyledContainer>
    );
}



export default withLocalizeStrings(TopNews);