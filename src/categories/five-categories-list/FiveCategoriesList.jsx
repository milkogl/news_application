import React from "react";
import styled from "styled-components";
import {Box, Flex} from "reflexbox/styled-components";
import ScrollMenu from 'react-horizontal-scrolling-menu';
import {NavLink} from "react-router-dom";

import routes from "../../config/routes";
import NewsCard from "../../news-card/NewsCard";
import {withLocalizeStrings} from "../../languages/Localize";
import {useApplicationStateValue} from "../../application/Application";

const StyledContainer = styled(Flex)`
  height: 100%;
  width: 100%;
  flex-flow: column;
  margin-bottom: 32px;

  && .horizontal-menu{
    width: 100%;
    height: fit-content;
    margin-top: 16px;
  }
  && .menu-wrapper{
    width: 90%;
  }
  && .menu-item-wrapper{
    margin-right: 16px;
  }
  && .scroll-menu-arrow--disabled {
    visibility: hidden;
  }

  && .expand-collapse-arrow {
    transform: rotate(90deg);
    transition: transform 0.1s ease 0s;
  }

  && .collapsed {
      display: none;
  }
`;

const StyledCategoryName = styled(NavLink)`
    font-size: 16px;
`;

const Arrow = ({ text }) => {
    return <div>{text}</div>;
};
  
const ArrowLeft = Arrow({ text: "<" });
const ArrowRight = Arrow({ text: ">"});
  
const onClickHandler = (collapsedCategories, setCollapsedCategories, category) => {
    const collapsedCategoriesNew = {...collapsedCategories};
    const currentCategoryCollapsed = collapsedCategoriesNew[category];
    collapsedCategoriesNew[category] = !currentCategoryCollapsed;
    setCollapsedCategories(collapsedCategoriesNew);
}

const FiveCategoriesList = ({strings, ...props}) => {
    const {categories, collapsedCategories, setCollapsedCategories} = useApplicationStateValue();

    const news = categories[props.category].slice(0, 5).map((article, idx)=> {
        return <NewsCard key={idx} article={article}/>;
    });

    const collapsedCategoriesCopy = {...collapsedCategories};
    const currentCategoryCollapsed = collapsedCategoriesCopy[props.category];

    return (
        <StyledContainer>
            <Flex>
                <StyledCategoryName to={`${routes.CATEGORY}${props.category}`}>{strings.categories[props.category]}</StyledCategoryName>
                <Box marginLeft="16px" className={currentCategoryCollapsed ? '' : 'expand-collapse-arrow'}  onClick={() => onClickHandler(collapsedCategories, setCollapsedCategories, props.category)}>></Box>
            </Flex>
            <Box className={currentCategoryCollapsed ? 'collapsed' : ''}>
                <ScrollMenu
                    data={news}
                    arrowLeft={ArrowLeft}
                    arrowRight={ArrowRight}
                    scrollBy={1}
                    hideSingleArrow={true}
                    wheel={false}
                />
            </Box>
        </StyledContainer>
    );
}

export default withLocalizeStrings(FiveCategoriesList);