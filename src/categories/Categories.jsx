import React from "react";
import styled from "styled-components";
import {Flex} from "reflexbox/styled-components";

import {useApplicationStateValue} from "../application/Application";
import Heading from "../components/Heading";
import {withLocalizeStrings} from "../languages/Localize";
import FiveCategoriesList from "./five-categories-list/FiveCategoriesList";

const StyledContainer = styled(Flex)`
  height: 100%;
  width: 100%;
  flex-flow: column;
`;

const Categories = ({strings}) => {
    const {region, categories} = useApplicationStateValue();

    const country = region === 'gb' ? strings.GB : strings.US; 
    const list = [];
    for (const [key] of Object.entries(categories)) {
        list.push(<FiveCategoriesList key={key}category={key}/>);
    }

    return (
        <StyledContainer>
            <Heading>{`${strings.categories.TITLE} ${country}`}</Heading>
            {list}
        </StyledContainer>
    );
}

export default withLocalizeStrings(Categories);