import React from "react";
import styled from "styled-components";
import {Box, Flex} from "reflexbox/styled-components";

import NewsCard from "../../news-card/NewsCard";
import {withLocalizeStrings} from "../../languages/Localize";
import {useApplicationStateValue} from "../../application/Application";
import Heading from "../../components/Heading";


const StyledContainer = styled(Flex)`
    flex-flow: column;
    width: 100%;
`;

const StyledTopNewsContainer = styled(Box)`
    height: 100%;
    width: 100%;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(270px, 0fr));
    grid-gap: 1rem;
    justify-content: center;
`;

const Category = ({strings, ...props}) => {
    const {categories, region} = useApplicationStateValue();
    const category = props.match && props.match.params && props.match.params.category;
    const articles = categories[category];

    const news = articles.map((article, idx)=> {
        return <NewsCard key={idx} article={article}/>;
    })

    const country = region === 'gb' ? strings.GB : strings.US; 
    return (
        <StyledContainer>
            <Heading>{`Top ${category} news from ${country}`}</Heading>
            <StyledTopNewsContainer>
                {news}
            </StyledTopNewsContainer>
        </StyledContainer>
    );
}

export default withLocalizeStrings(Category);