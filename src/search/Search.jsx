import React, {useState} from 'react';
import styled from "styled-components";
import {Flex, Box} from "reflexbox/styled-components";
import { Input } from 'antd';

import {withLocalizeStrings} from "../languages/Localize";
import {useApplicationStateValue} from "../application/Application";
import NewsCard from "../news-card/NewsCard";
import {getSearchData} from "../services/fetch/search.service";
import Heading from "../components/Heading";

const { Search } = Input;

const StyledContainer = styled(Flex)`
    flex-flow: column;
    width: 100%;
`;

const StyledSearchInput = styled(Search)`
    margin-top: 16px;
    height: 50px;
    width: 50vw;
    align-self: center;
`;

const StyledTopNewsContainer = styled(Box)`
    margin-top: 16px;
    height: 100%;
    width: 100%;
    display: grid;
    grid-template-columns: repeat(auto-fill, minmax(270px, 0fr));
    grid-gap: 1rem;
    justify-content: center;
`;

const searchNews = (region, searchText, setSearchResult) => {

    getSearchData(region, searchText).then(({data}) => {
        const tempData = {...data};
          setSearchResult([...tempData.articles]);
    });
}

const SearchElem = ({strings, ...props}) => {
    const {region} = useApplicationStateValue();
    const [searchResult,setSearchResult] = useState([]);

    let news = searchResult.map((article, idx)=> {
        return <NewsCard key={idx} article={article}/>;
    });
    if(news.length < 1) {
        news = strings.search.NO_RESULTS;
    }

    React.useEffect(() => {
        setSearchResult([]);
    }, [region]);

    const country = region === 'gb' ? strings.GB : strings.US; 

    return(
    
    <StyledContainer>
        <Heading>{`${strings.topnews.TITLE} ${country}`}</Heading>
        <StyledSearchInput
            placeholder="Search term..."
            onSearch={value => searchNews(region, value, setSearchResult)}
            enterButton 
        />
        <StyledTopNewsContainer>
                {news}
            </StyledTopNewsContainer>
    </StyledContainer>)
}

export default withLocalizeStrings(SearchElem);