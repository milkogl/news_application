import React from "react";
import PropTypes from "prop-types";

import LocalizedStrings from "react-localization";
import stringsJson from "./languages.json";

export const strings = new LocalizedStrings(stringsJson);

export const LocalizeContext = React.createContext({
    strings,
    currentLanguage: "en",
    setLanguage: () => {},
});
export const LocalizeConsumer = LocalizeContext.Consumer;

export const LocalizeProvider = props => {
    const [currentLanguage, setCurrentLanguage] = React.useState("en");
    //we can potentially add support for any other language
    const setLanguage = locale => {
        locale = "en";
        localStorage.setItem("language", "en");
        strings.setLanguage(locale);
        setCurrentLanguage(locale);
    };
    const [globalState,setGlobalState] = React.useState({
        strings,
        currentLanguage,
        setLanguage,
    })


    React.useEffect(() => {
        setLanguage(localStorage.getItem("language"));
        setGlobalState(
            {
            strings,currentLanguage,setLanguage
        });
    }, [currentLanguage]);


    return (
        <LocalizeContext.Provider value={globalState}>
            {props.children}
        </LocalizeContext.Provider>
    );
};

LocalizeProvider.propTypes = {
    children: PropTypes.node.isRequired,
};

export const useLocalizeStateValue = () => React.useContext(LocalizeContext);

export const withLocalizeStrings = Component => {
    return props => {
        const { strings } = React.useContext(LocalizeContext);
        return <Component strings={strings} {...props}/>;
    };
};
