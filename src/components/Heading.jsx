import React from "react";
import styled from "styled-components";
import {Box} from "reflexbox/styled-components";

const StyledHeading = styled(Box)`
    font-weight: 700;
    font-size: 24px;
`;

const Heading = (props) => {
    return <StyledHeading>
        {props.children}
    </StyledHeading>
}

export default Heading;